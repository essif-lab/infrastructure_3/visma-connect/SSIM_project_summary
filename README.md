SSI Mandate project summary
===================


# Introduction
The current digital infrastructure for mandates is not without problems. In many cases providing a mandate digitally is not possible. Other systems still require a wet signature on a PDF which can easily be falsified. The digital systems out there are fragmented and siloed between various service providers with a poor user experience. As a result people share their passwords rather than actually providing mandates resulting in insecure systems. To make matters worse most people use similar passwords for different systems increasing the attack vectors to compromise the identity and as a result also the identity of the person providing a mandate. 
By providing mandates using SSI we can provide a solution to the problems mentioned above. 

First of all SSI mandates solve the problem of silo solutions because it is a neutral technology not controlled by a central party, rather data is managed by the identities themselves. This solves compliance problems and potential distrust between different parties and hence lowers the barrier for different services to adopt SSI as opposed to centralized solutions. 

Secondly because SSI is decentralized and user data isn’t stored in local databases the amount of attack vectors are lowered and decreases the incentive for hackers to try and hack these systems. In a central database a hacker only needs to hack one single point of failure to gather the data of potential millions of customers whereas in a decentralized system such as SSI the same amount of effort is required to get the data of a single customer which is simply not as interesting. 


# Summary
Our SSI mandate service is a generic and holistic approach to provide and request mandates. Mandates are SSI credentials. The SSI Mandate service issues a mandate on behalf of a provider for a holder (which becomes the "authorized representative"). These credentials can be used to prove to a verifier that the authorized representative is authorized to act for specific actions on behalf of the provider. The mandate credentials are stored in the wallet of the authorized representative as opposed to a central database in current systems. The provider can revoke this credential at any point in time if he/she no longer wants the authorized representative to act on their behalf by updating a revocation hash on the blockchain. The SSI mandate provides mandates completely peer to peer and isn’t limited to individuals only. A SSI wallet can also represent a device or institution, for example a company can use a SSI company wallet to authorize employees to access the building or use the company credit card up to a certain amount. 

Overall, mandates can be used for 2 different purposes:
- To authorize someone to have acess to data of someone else.
- To authorize someone to act on behalf of someone else.

# Example solutions
The SSI Mandate Service is already used in two different cases. Both cases have a thoroughly elaborated problem. Both will be described here:
- Cross Border Delegation (Customs): Authorizing a company (holder) to do the customs administration on behalf of an importer from the Netherlands (provider).
- Construction site access (Heras Hekwerken): Authorizing a subcontractor or self-employed (holder) to have access to a construction site to be able to work for a main contractor (provider).


## Construction site access
Heras Hekwerken is a well known company within the Netherlands, mainly because of the production of fences. But they are also one of the biggest suppliers of saftey and security solutions of construction sites. A major problem on construction sites is unauthorized or unqualified personnel present on such construction sites. Key cards are shared with coworkers or lent with malicious intentions. To face this problem, we've developed together with Heras this Mandate service.
The owner of the construction site will mandate the main contractor to take care of the complete authorization flow.
With this mandate the main contractor can provide subcontractors with several other mandates. One kind of mandate is just having access to the construction site. Another kind of mandate enables a subcontractor to provide other contractors or workers to gain access. Every provided mandate is related to the mandate which is used to provide this mandate. This results in a tree of mandates. There is no limit to the length of a branch within a tree.
Every mandate can be revoked. This immediately ends the validity of the mandate and all its child mandates.
In this example it becomes clear that one user can have a certain role in one point of the process, but another role later on. For instance a subcontractor is a holder of a mandate which provides them access to the construction site. When the provide a mandate to one of their workers, the subcontractor acts as a provider.



# SSI Mandate project
The current SSI Mandate Service is described in the [functional specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/functional_specification.md). The specification describes the flow of defining, creating, accepting and proving a mandate. Next to that, the architecture of the SSI Mandate Service describes the components and the relation with the SSI Platform and the different users. The [interface specification](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/interface_specification.md) describes how you can use the Mandate Service using an API.
The backend application has been built with Hyperleder Indy and uses Trinsic as wallet provider. We want to make the application more interoperable, trustworthy and robust. 

**INTEROPERABILITY** We want to make a mandate service that is operable with different types of wallets.

**ROBUST** We want to make the mandate service more robust by adding complex validation of mandates.

**TRUSTWORTHY** We make the mandate service more trustworthy by adding identification of the parties involved in the mandate process.


In the [envisioned interoperability with others](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/envisioned_interoperability_with_others.md) we describe with what parties we want to cooperate to achieve the abovementioned goals. 

The [architecture](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Architecture.md) describes what components are involved in the solution.
Next to that, we have a separate [terminology](https://gitlab.grnet.gr/essif-lab/infrastructure_3/visma-connect/deliverables/-/blob/master/Terminology.md) overview to clarify the terms we use.



